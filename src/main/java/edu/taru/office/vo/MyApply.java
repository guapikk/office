package edu.taru.office.vo;

import java.util.Date;

/**
 * 我的申请
 */
public class MyApply {


    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    /**
     * 业务关键Key，通常是业务主键
     */
    String  businessKey;

    /**
     * 发起人编号
     */

    String  userId;
    /**
     * 发起人名称
     */
    String  user;



    String  processId;



    //流程定义的定义的名称
    String  processDefinitionName;
    String  processDefinitionKey;
    String  processDefinitionId;
    /**
     * 流程的标题
     */

    String title;

    /**
     * 创建时间
     */
    Date createDate;

    /**
     * 审批的类型(请假、飞机、差旅 )，根据类型不同，跳转到不同额审批表单页面
     */
    String   category;
}
