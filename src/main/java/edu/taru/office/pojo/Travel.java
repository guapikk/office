package edu.taru.office.pojo;

import java.util.Date;

public class Travel {
    private Integer id;

    private Integer uid;
    //用户姓名
    private String uname;

    private Date sdate;

    private Date edate;

    private String reason;

    private String startoff;

    private String arrived;

    private String traffic;

    private String other;
    private Integer deptId;
    private String  dname;

    private String title;
    private String processId;
    private Double  borrow;

    private String  result;//审批结果
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }



    public Double getBorrow() {
        return borrow;
    }

    public void setBorrow(Double borrow) {
        this.borrow = borrow;
    }




    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }





    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname == null ? null : uname.trim();
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public Date getEdate() {
        return edate;
    }

    public void setEdate(Date edate) {
        this.edate = edate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public String getStartoff() {
        return startoff;
    }

    public void setStartoff(String startoff) {
        this.startoff = startoff == null ? null : startoff.trim();
    }

    public String getArrived() {
        return arrived;
    }

    public void setArrived(String arrived) {
        this.arrived = arrived == null ? null : arrived.trim();
    }

    public String getTraffic() {
        return traffic;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic == null ? null : traffic.trim();
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other == null ? null : other.trim();
    }
}