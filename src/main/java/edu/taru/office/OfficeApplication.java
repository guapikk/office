package edu.taru.office;

import org.activiti.spring.boot.EndpointAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@MapperScan("edu.taru.office.mapper")
@ImportResource(locations={"classpath:kaptcha.xml"})
public class OfficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfficeApplication.class, args);
	}
}
