package edu.taru.office.service;

import edu.taru.office.pojo.Dept;

import java.util.List;

/**
 * Created by HuKang on 2018/4/26.
 */
public interface DeptService {
    /**
     * 查询部门列表
     * @return
     */
    List<Dept> selectList();

    /**
     * 添加部门
     * @param deptname
     * @return
     */
    int selectRepeat(String deptname);

    /**
     * 添加部门
     * @param record
     * @return
     */
    int insertSelective(Dept record);

    /**
     * 删除部门
     * @param deptid
     * @return
     */
    int deleteByPrimaryKey(Integer deptid);

    /**
     * 部门修改是对部门名称是否重复进行判断
     * @param dept
     * @return
     */
    int updateRepeat(Dept dept);

    /**
     * 部门修改
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Dept record);

    /**
     * 根据部门id查询部门信息用作修改
     * @param deptid
     * @return
     */
    Dept selectByPrimaryKey(Integer deptid);

    /**
     * 根据deptid批量删除dept
     * @param id
     * @return
     */
    void deleteListById(String [] id);


    /**
     * 查询我的部门根据登录我的ID
     * @param uid
     * @return
     */
    Dept  selectMyDept(String uid);









}
