package edu.taru.office.service.impl;

import edu.taru.office.mapper.ProcessMapper;
import edu.taru.office.service.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessServiceImpl  implements ProcessService {
    @Autowired
    ProcessMapper  processMapper;
    @Override
    public List<String> selectRoleIds(String id) {
        return processMapper.selectRoleIdsByUserId(Integer.valueOf(id));
    }
}
