package edu.taru.office.service.impl;

import edu.taru.office.mapper.RoleMapper;
import edu.taru.office.pojo.Role;
import edu.taru.office.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;
    /**
     * 查询所有角色
     */
    public List<Role> selectList(){
      List<Role>  list = roleMapper.selectList();
      return list;
    }

    public int insertSelective(Role record){
        return roleMapper.insertSelective(record);
    }

    public int selectRepeat(String rolename){
        return roleMapper.selectRepeat(rolename);
    }

    public int deleteByPrimaryKey(Integer roleid){
        return roleMapper.deleteByPrimaryKey(roleid);
    }

    public Role selectByPrimaryKey(Integer roleid){
        Role role = roleMapper.selectByPrimaryKey(roleid);
        return role;
    }
    public int updateRepeat(Role role){
        return roleMapper.updateRepeat(role);
    }
    public int updateByPrimaryKeySelective(Role record){
        return roleMapper.updateByPrimaryKeySelective(record);
    }

    public void  deleteByPrimaryKey(String [] checked){
        for(int i=0;i<checked.length;i++){
            Integer roleid = Integer.valueOf(checked[i]);
            roleMapper.deleteByPrimaryKey(roleid);
        }
    }

    public void insertUrl(String roleid ,String [] checked ){
        for(int i=0; i<checked.length;i++){
            Integer urlid = Integer.valueOf(checked[i]);
            Integer id = Integer.valueOf(roleid);
            roleMapper.insertUrl(id,urlid);
        }
    }


}
