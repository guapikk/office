package edu.taru.office.service.impl;

import edu.taru.office.mapper.DeptMapper;
import edu.taru.office.pojo.Dept;
import edu.taru.office.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by HuKang on 2018/4/26.
 */

@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    DeptMapper deptMapper;

    @Override
    public List<Dept> selectList() {
        List<Dept> list = deptMapper.selectList();
        return list;
    }

    @Override
    public int selectRepeat(String deptname) {
        int result = deptMapper.selectRepeat(deptname);
        return result;
    }

    @Override
    public int insertSelective(Dept record) {
        return deptMapper.insertSelective(record);
    }

    @Override
    public int deleteByPrimaryKey(Integer deptid) {
        return deptMapper.deleteByPrimaryKey(deptid);
    }

    @Override
    public int updateRepeat(Dept dept) {
        int result = deptMapper.updateRepeat(dept);
        return result;
    }

    @Override
    public int updateByPrimaryKeySelective(Dept record) {
        return deptMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public Dept selectByPrimaryKey(Integer deptid) {
        Dept dept = deptMapper.selectByPrimaryKey(deptid);
        return dept;
    }

    @Override
    public void deleteListById(String[] id) {
        for (int i=0;i<id.length;i++){
            deptMapper.deleteByPrimaryKey(Integer.valueOf(id[i]));
        }
    }

    @Override
    public Dept selectMyDept(String uid) {
        return deptMapper.selectDeptByUserId(Integer.valueOf(uid));
    }
}
