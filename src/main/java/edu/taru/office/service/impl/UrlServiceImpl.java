package edu.taru.office.service.impl;

import edu.taru.office.mapper.UrlMapper;
import edu.taru.office.pojo.Url;
import edu.taru.office.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 *
 */
@Service
public class UrlServiceImpl implements UrlService{
    @Autowired
    UrlMapper urlMapper;
    /**
     * 查询所有菜单
     * @return
     */
    public List<Url> selectList(){
        List<Url> list = urlMapper.selectList();
        return list;
    }
    public  List<Url> havaNotMenu(Integer roleid){
        List<Url> list = urlMapper.havaNotMenu(roleid);
        return list;
    }
    public List<Url> qryMenuTree(){
        List<Url> list = urlMapper.qryMenuTree();
        return list;
    }
    public  List<Url> qryRoleExistTree(Integer roleid){
        List<Url> list = urlMapper.qryRoleExistTree(roleid);
        return list;
    }
    public  int deleteByPrimaryKey(Integer urlid){
        return  urlMapper.deleteByPrimaryKey(urlid);
    }
    public Url selectByPrimaryKey(Integer urlid){
        Url url = urlMapper.selectByPrimaryKey(urlid);
        return  url;
    }

    public int updateRepeat(Url url){
        int result = urlMapper.updateRepeat(url);
        return result;
    }
    public  int updateByPrimaryKeySelective(Url record){
        return urlMapper.updateByPrimaryKeySelective(record);
    }

    @Transactional
    public  void deleteListById(String [] id){
        for (int i=0; i<id.length;i++){
            urlMapper.deleteByPrimaryKey(Integer.valueOf(id[i]));
        }
    }
    public int selectRepeat(String title){
        int result = urlMapper.selectRepeat(title);
        return  result;
    }
    public int insertSelective(Url record){
        return urlMapper.insertSelective(record);
    }
}
