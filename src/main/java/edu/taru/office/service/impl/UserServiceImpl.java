package edu.taru.office.service.impl;

import edu.taru.office.mapper.UserInfoMapper;
import edu.taru.office.mapper.UserMapper;
import edu.taru.office.pojo.User;
import edu.taru.office.pojo.UserInfo;
import edu.taru.office.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 用户管理
 */
@Service
public class UserServiceImpl  implements UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserInfoMapper userInfoMapper;
    /**
     * 登录功能
     * @param username
     * @param password
     * @return
     */
    public User login(String username , String password){
        User  temp  =new User();
        temp.setUsername(username);
        temp.setPassword(password);
        User  user =userMapper.selectByAuthc(temp);
        return user;
    }
    /**
     * 查询所有用户
     */
    public List<User> selectList (){
        List <User> list = userMapper.selectList();
        return list ;
    }
    public int selectRepeat(String username){
        int result = userMapper.selectRepeat(username);
        return result;
    }
    /**
     * 添加用户
     */
    public int addUser(User record){

        return userMapper.insertSelective(record);
    }

    public int deleteByPrimaryKey (Integer id){
        return userMapper.deleteByPrimaryKey(id);
    }

    public void deleteListById(String [] id){
        for(int i=0;i<id.length;i++){
            Integer ids = Integer.valueOf(id[i]);
            userMapper.deleteByPrimaryKey(ids);
        }
    }

    public User selectByPrimaryKey(Integer id ){
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }

    public int updateByPrimaryKeySelective(User user ){
        return userMapper.updateByPrimaryKeySelective(user);
    }

    public List<HashMap> selectRoleByUserId(Integer userid){
        List list = userMapper.selectRoleByUserId(userid);
        return list;
    }

    public void insertRole(String id,String [] checked){
        for(int i=0;i<checked.length;i++){
            Integer userid = Integer.valueOf(id);
            Integer roleid = Integer.valueOf(checked[i]);
            userMapper.insertRole(userid,roleid);
        }
    }

    public List<HashMap> qryExistRole(Integer userid){
        List list = userMapper.qryExistRole(userid);
        return list;
    }

    public int updateRepeat(User user){
        return userMapper.updateRepeat(user);
    }

    public int insertUserinfo(UserInfo userinfo){
        return userInfoMapper.insertSelective(userinfo);
    }
    public int deleteByUserInfoPrimaryKey(Integer id){
        return userInfoMapper.deleteByPrimaryKey(id);
    }

    public  void deleteUserinfoListById(String []id){
        for(int i=0;i<id.length;i++){
            Integer ids = Integer.valueOf(id[i]);
            userInfoMapper.deleteByPrimaryKey(ids);
        }
    }

    public HashMap<String ,Object> qryUserInfo(Integer id){
        HashMap<String ,Object> list = userMapper.qryUserInfo(id);
        return list;
    }

    @Override
    public int changePwd(User user) {
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public HashMap<String, Object> qryUserForMain(Integer id) {
        return userMapper.qryUserForMain(id);
    }
}
