package edu.taru.office.service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by HuKang on 2018/5/15.
 */
public interface AttendanceService {
    /**
     * 查询用户签到信息
     * @param uid
     * @return
     */
    List<HashMap> qryUserSignIn(Integer uid);
    /**
     * 查询用户签退信息
     * @param uid
     * @return
     */
    List<HashMap> qryUserSignBack(Integer uid);
    /**
     * 判断是否已签到
     * @param uid
     * @return
     */
    HashMap selectSignIn(Integer uid);
    /**
     * 签到
     * @param uid
     */
    void SignIn(Integer uid);

    /**
     * 签退
     * @param uid
     */
    void SignBack(Integer uid);

}
