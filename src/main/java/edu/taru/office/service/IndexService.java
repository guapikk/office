package edu.taru.office.service;

import edu.taru.office.pojo.Url;

import java.util.List;

public interface IndexService {


    /**
     * 查询菜单
     * @param username
     * @return
     */
    public List<Url> selectMenus(String username);
}
