package edu.taru.office.service;

import java.util.List;

public interface ProcessService {
    /**
     * 查询角色IDs
     * @param id
     * @return
     */
    public List<String> selectRoleIds(String id);
}
