package edu.taru.office.service;

import edu.taru.office.pojo.User;
import edu.taru.office.pojo.UserInfo;

import java.util.HashMap;
import java.util.List;

public interface UserService {

    User login(String username , String password);
    /**
     * 查询所有用户
     * @return
     */
    List<User> selectList();
    /**
     * 添加用户时判断用户名是否存在
     * @param username
     * @return
     */
    int selectRepeat(String username);
    /**
     * 添加用户
     * @param record
     * @return
     */
    int addUser(User record);

    /**
     * 根据id删除用户
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 根据用户id批量删除user
     * @param id
     * @return
     */
    void deleteListById(String [] id);

    /**
     * 根据用户id对userinfo表进行批量删除对应user表
     * @param id
     */
    void deleteUserinfoListById(String []id);

    /**
     * 根据id查询用户 用作修改
     * @param id
     * @return
     */
    User selectByPrimaryKey(Integer id);

    /**
     * 用户修改 根据id
     * @param user
     * @return
     */
    int updateByPrimaryKeySelective(User user);

    /**
     * 根据用户id查询 用户未拥有的角色做分配权限使用
     * @param userid
     * @return
     */
    List<HashMap> selectRoleByUserId(Integer userid);

    /**
     * 给用户分配权限（角色）
     * @param id
     * @param
     * @return
     */
    void insertRole(String  id,String [] checked);

    /**
     * 根据用户id查询用户已有角色
     * @param userid
     * @return
     */
    List<HashMap> qryExistRole(Integer userid);

    /**
     * 用户修改时判断修改的用户名是否存在，如果未修改用户名则可以存在
     * @param user
     * @return
     */
    int updateRepeat(User user);

    /**
     * 像用户详细表中插入数据
     * @param userinfo
     * @return
     */
    int insertUserinfo(UserInfo userinfo);

    /**
     * 用户详细信息删除
     * @param id
     * @return
     */
    int deleteByUserInfoPrimaryKey(Integer id);

    /**
     * 查询用户详细信息
     * @param id
     * @return
     */
    HashMap<String ,Object> qryUserInfo(Integer id);

    /**
     * 修改密码
     * @param user
     * @return
     */
    int changePwd(User user);

    /**
     * 给主页面查询用户基本信息
     * @param id
     * @return
     */
    HashMap<String,Object> qryUserForMain(Integer id);


}
