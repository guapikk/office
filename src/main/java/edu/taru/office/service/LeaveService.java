package edu.taru.office.service;

import edu.taru.office.pojo.Leave;

/**
 * Created by HuKang on 2018/5/5.
 */
public interface LeaveService {
    /**
     *添加一个请假单
     * @param record
     * @return
     */
    int insert(Leave record);

    /**
     * 查询请假单
     * @param busid
     * @return
     */
    Leave selectLeave(String busid);

    /**
     * 审批结束后，更新审批意见
     * @param result
     * @param id
     * @return
     */
    int  updateResultByKey(String result,String id);

    /**
     * 查询预览
     * @param id
     * @return
     */
    Leave selectByPrimaryKey(Integer id);
}
