package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.pojo.Url;
import edu.taru.office.service.IndexService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 首页管理
 */
@RestController
public class IndexController {

    @Autowired
    IndexService indexService;

    /**
     * 根据当前用户查询菜单查询
     * @return
     */
    @RequestMapping("/index/qmenu")
    @ResponseBody
    public   Object index(){
        Subject subject = SecurityUtils.getSubject();
        String   username =String.valueOf(subject.getPrincipal());
        JsonResult<List<Url>> jsonResult =null;
        try
        {
            List<Url> menus =indexService.selectMenus(username);
            jsonResult =new JsonResult<List<Url>>(Constants.STATUS_OK,"查询菜单成功",menus);
        }catch (Exception ex){
            jsonResult =new JsonResult<List<Url>>(Constants.STATUS_ERROR,"查询菜单异常");
        }
        return  jsonResult;
    }




}
