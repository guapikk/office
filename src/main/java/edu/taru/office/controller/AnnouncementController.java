package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.pojo.Announcement;
import edu.taru.office.service.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**公告管理
 * Created by HuKang on 2018/5/6.
 */
@Controller
public class AnnouncementController {
    @Autowired
    AnnouncementService announcementService;

    /**
     * 查询公告列表
     * @param model
     * @return
     */
    @RequestMapping("/announcement/list")
    public  String qryAnnouncementList(Model model){
        List<Announcement> list = announcementService.selectList();
        model.addAttribute("list",list);
        return "system/AnnouncementList";
    }

    /**
     * 查询公告详情
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/announcement/qryInfo")
    public String qryAnnouncementInfo(String id,Model model){
        System.out.println(id);
        Announcement announcement = announcementService.selectByPrimaryKey(Integer.valueOf(id));
        model.addAttribute("announcement",announcement);
        return "Announcement";
    }

    /**
     * 添加公告
     * @param announcement
     * @return
     */
    @RequestMapping("/announcement/add")
    @ResponseBody
    public Object announcementAdd(Announcement announcement){
        JsonResult<String> jsonResult=null;
        String title = announcement.getTitle();
        String content = announcement.getContent();
        try {
            announcementService.insertAnnouncement(title,content);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"添加成功");
        }catch (Exception e){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"添加失败");
        }
        return  jsonResult;
    }

    @RequestMapping("/announcement/delete")
    @ResponseBody
    public Object announcementDelete(String [] checked){
        JsonResult<String> jsonResult=null;
        try {
            announcementService.deleteAnnouncement(checked);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return  jsonResult;
    }

}
