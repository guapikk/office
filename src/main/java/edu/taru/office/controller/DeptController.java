package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.pojo.Dept;

import edu.taru.office.service.DeptService;
import edu.taru.office.util.UserUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by HuKang on 2018/4/26.
 */
@Controller
public class DeptController{
    @Autowired
    DeptService deptService;

    /**
     * 查询部门列表
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/dept/list")
    public String qyrDeptList(HttpServletRequest request, HttpServletResponse response){
        List<Dept> list = deptService.selectList();
        request.setAttribute("dept",list);
        return "system/dept";
    }


    @RequestMapping("/dept/add")
    @ResponseBody
    public Object AddUser(Dept dept, HttpServletRequest request, HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            int result = deptService.selectRepeat(dept.getDeptname());
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"部门名称已存在");
            }else{
                deptService.insertSelective(dept);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"添加成功");
            }
        }catch (Exception e ){
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"添加失败");

        }
        return jsonResult;
    }

    /**
     * 删除部门
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/dept/delete")
    @ResponseBody
    public Object  delDept(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        JsonResult<String> jsonResult = null;
        try {
            deptService.deleteByPrimaryKey(id);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }



    /**
     * 根据 用户ID查询所在部门列表
     * @param
     * @param
     * @return
     */
    @RequestMapping("/dept/mydept")
    @ResponseBody
    public Object  qryMydept(HttpSession session){
        String uid = UserUtil.getSessionUserFromSession(session).getUserid();
        JsonResult< Dept> jsonResult = null;
        try {
            Dept  dept =deptService.selectMyDept(uid);
            jsonResult = new JsonResult< Dept>(Constants.STATUS_OK,"查询成功",dept);
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<Dept>(Constants.STATUS_ERROR,"查询异常");
        }
        return jsonResult;
    }




    /**
     * 根据id查询 菜单用作修改
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/dept/selectById")
    public String selectByPrimaryKey(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        Dept dept = deptService.selectByPrimaryKey(id);
        request.setAttribute("dept",dept);
        return "system/updateDept";
    }

    /**
     * 部门修改对部门名称是否重复进行判断
     * @param dept
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/dept/update")
    @ResponseBody
    public Object UpdateUser(Dept dept,HttpServletRequest request,HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            int result = deptService.selectRepeat(dept.getDeptname());
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"部门名称已存在");
            }else{
                deptService.updateByPrimaryKeySelective(dept);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"修改成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"修改失败");
        }
        return jsonResult;
    }

    /**
     * 对菜单进行批量删除
     * @param checked
     * @return
     */
    @RequestMapping("/dept/deleteList")
    @ResponseBody
    public Object  deleteList(String [] checked){
        JsonResult<String> jsonResult = null;
        try {
            deptService.deleteListById(checked);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){

            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }
}
