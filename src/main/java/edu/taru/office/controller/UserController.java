package edu.taru.office.controller;


import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.common.SessionUser;
import edu.taru.office.pojo.User;
import edu.taru.office.pojo.UserInfo;
import edu.taru.office.service.UserService;
import edu.taru.office.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 * 用户列表Controller类 用于对用户表的增删改查的操作
 */
@Controller
public class UserController {
    @Autowired
    UserService userService;

    /**
     * 查询用户
     */
    @RequestMapping("/user/list")
    public String QryUser(HttpServletRequest request, HttpServletResponse response){
        List <User> list = userService.selectList();
        request.setAttribute("list",list);
        return "system/users";
    }
    /**
     * 新增用户
     */
    @RequestMapping("/user/add")
    @ResponseBody
    public Object AddUser(User user, HttpServletRequest request,HttpServletResponse response){
       JsonResult<String> jsonResult = null;
        try {
            int result = userService.selectRepeat(user.getUsername());
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"用户名已存在");
            }else{
                userService.addUser(user);
                //获取用户插入的ID
                Integer  id  =user.getId();
                System.out.println("****************"+id);
                //插入详细表
                UserInfo userinfo=new UserInfo();
                userinfo.setId(id);
                userService.insertUserinfo(userinfo);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"添加成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"添加失败");
        }
        return jsonResult;
    }
    /**
     * 删除用户
     */
    @RequestMapping("/user/delete")
    @ResponseBody
    public Object  DelUser(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        JsonResult<String> jsonResult = null;
        try {
            userService.deleteByPrimaryKey(id);
            userService.deleteByUserInfoPrimaryKey(id);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){

            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }

    /**
     * 根据用户id批量删除
     * @param checked
     * @return
     */
    @RequestMapping("/user/deleteList")
    @ResponseBody
    public Object  deleteList(String [] checked){
        JsonResult<String> jsonResult = null;
        try {
            userService.deleteListById(checked);
            userService.deleteUserinfoListById(checked);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){

            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }

    /**
     * 根据id查询 用户用作修改
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/user/selectById")
    public String selectByPrimaryKey(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        User user = userService.selectByPrimaryKey(id);
        request.setAttribute("user",user);
        return "system/updateUser";
    }
    /**
     * 用户修改会进行对用户名是否存在进行判断
     */
    @RequestMapping("/user/update")
    @ResponseBody
    public Object UpdateUser(User user,HttpServletRequest request,HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            int result = userService.updateRepeat(user);
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"用户名已存在");
            }else{
                userService.updateByPrimaryKeySelective(user);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"修改成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"修改失败");
        }
        return jsonResult;
    }

    /**
     * 根据用户id查询 用户未拥有的角色做分配权限使用
     * @param request
     * @param response
     * @return 返回到用户角色页面
     */
    @RequestMapping("/user/selectRoleByUserId")
    public String selectRoleByUserId(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        List<HashMap> list = userService.selectRoleByUserId(id);
        request.setAttribute("id",id);
        request.setAttribute("list",list);
        return "system/empower";
    }

    /**
     * 用户分配权限 表user_role 中插入 userid 和 roleid
     * @param id
     * @param checked
     * @return
     */
    @RequestMapping("/user/empower")
    @ResponseBody
    public Object UpdateUser(String id,String [] checked){
        JsonResult<String> jsonResult = null;
        try {
                userService.insertRole(id,checked);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"分配成功");
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"分配失败");
        }
        return jsonResult;
    }

    /**
     * 查询用户已有角色
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/user/qryExistRole")
    public String qryExistRole(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        List<HashMap> list = userService.qryExistRole(id);
        request.setAttribute("qryExistRole",list);
        return "system/qryExistRole";
    }

    /**
     * 查询用户详细信息修改时使用
     * @param session
     * @return
     */
    @RequestMapping("/qry/userinfo")
    public String qryUserInfo(HttpServletRequest request,HttpSession session){
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        HashMap<String ,Object> list = userService.qryUserInfo(Integer.valueOf(user.getUserid()));
        request.setAttribute("userinfo",list);
        return "system/userinfo";
    }


    /**
     * 修改密码
     * @param request
     * @param session
     * @return
     */
    @RequestMapping("/user/changePwd")
    @ResponseBody
    public Object changePwd(HttpServletRequest request,HttpSession session ,HttpServletResponse response){
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        String pwd = request.getParameter("newPwd");
        User u = new User();
        u.setId(Integer.valueOf(user.getUserid()));
        u.setPassword(pwd);
        System.out.println(pwd+"****************");
        System.out.println(user.getUserid()+"********************");
        JsonResult<String> jsonResult = null;
        try {
            userService.changePwd(u);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"修改成功");
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"修改失败");
        }
        return jsonResult;
    }



}
