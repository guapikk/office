package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.JsonResult;
import edu.taru.office.pojo.Role;
import edu.taru.office.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by HuKang on 2018/4/19.
 * 角色列表类 用于对角色的增删改查，以及赋菜单
 */
@Controller
public class RoleController {
    @Autowired
    RoleService roleService;

    /**
     * 查询角色列表
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/role/list")
    public String QryRole(HttpServletRequest request, HttpServletResponse response){
        List<Role> list = roleService.selectList();
        request.setAttribute("list",list);
        return "system/role";
    }

    /**
     * 添加角色（会对角色名称是否重复进行判断如果重复则无法添加）
     * @param role
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/role/add")
    @ResponseBody
    public Object AddUser(Role role, HttpServletRequest request,HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            int result = roleService.selectRepeat(role.getRolename());
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"该角色已存在");
            }else{
                roleService.insertSelective(role);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"添加成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"添加失败");
        }
        return jsonResult;
    }

    /**
     * 删除角色
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/role/delete")
    @ResponseBody
    public Object  DelUser(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        JsonResult<String> jsonResult = null;
        try {
            roleService.deleteByPrimaryKey(id);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){

            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }

    /**
     * 根据角色id查询角色用作修改
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/role/selectById")
    public String selectByPrimaryKey(HttpServletRequest request,HttpServletResponse response){
        Integer id = Integer.valueOf(request.getParameter("id"));
        Role role = roleService.selectByPrimaryKey(id);
        request.setAttribute("role",role);
        return "system/updateRole";
    }

    /**
     * 角色修改会进行对角色名称是否存在进行判断
     * @param role
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/role/update")
    @ResponseBody
    public Object UpdateUser(Role role,HttpServletRequest request,HttpServletResponse response){
        JsonResult<String> jsonResult = null;
        try {
            int result = roleService.updateRepeat(role);
            if(result!=0){
                jsonResult = new JsonResult<String>(Constants.STATUS_NO_DATA,"角色名称已存在");
            }else{
                roleService.updateByPrimaryKeySelective(role);
                jsonResult = new JsonResult<String>(Constants.STATUS_OK,"修改成功");
            }
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"修改失败");
        }
        return jsonResult;
    }
    /**
     * 根据用户id批量删除
     * @param checked
     * @return
     */
    @RequestMapping("/role/deleteList")
    @ResponseBody
    public Object  deleteList(String [] checked){
        JsonResult<String> jsonResult = null;
        try {
            roleService.deleteByPrimaryKey(checked);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"删除成功");
        }catch (Exception e ){
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"删除失败");
        }
        return jsonResult;
    }

    /**
     * 角色分配菜单 表role_url中插入 roleid 和 urlid[]
     * @param id
     * @param checked
     * @return
     */
    @RequestMapping("/role/insertUrl")
    @ResponseBody
    public Object insertUrl(String roleid,String [] checked){
        JsonResult<String> jsonResult = null;
        try {
            roleService.insertUrl(roleid,checked);
            jsonResult = new JsonResult<String>(Constants.STATUS_OK,"分配成功");
        }catch (Exception e ){
            e.printStackTrace();
            jsonResult = new JsonResult<String>(Constants.STATUS_ERROR,"分配失败");
        }
        return jsonResult;
    }

}
