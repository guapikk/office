package edu.taru.office.controller;

import edu.taru.office.common.Constants;
import edu.taru.office.common.SessionUser;
import edu.taru.office.pojo.Announcement;

import edu.taru.office.service.AnnouncementService;
import edu.taru.office.service.UserService;
import edu.taru.office.util.StringUtil;
import edu.taru.office.util.UserUtil;
import edu.taru.office.vo.MyApply;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**主页面业务操作
 * Created by HuKang on 2018/5/6.
 */
@Controller
public class MainController {
    @Autowired
    UserService userService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    AnnouncementService announcementService;

    /**
     * 主页面查询用户基本信息和系统公告
     * @param session
     * @return
     */
    @RequestMapping("/main/qryUserForMain")
    public String qryUserInfo(HttpSession session , Model model){
        SessionUser user = UserUtil.getSessionUserFromSession(session);
        HashMap<String,Object> map = userService.qryUserForMain(Integer.valueOf(user.getUserid()));
        HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery();
        List<HistoricProcessInstance>   processInstances = query.includeProcessVariables().
                startedBy(user.getUserid()).orderByProcessInstanceStartTime().desc().unfinished().list();
        List<MyApply>  myApplys =new ArrayList<>();
        if(processInstances!=null &&processInstances.size()>0 ){
            for(HistoricProcessInstance hp:processInstances){
                MyApply myApply =new MyApply();
                myApply.setBusinessKey(hp.getBusinessKey());
                myApply.setProcessId(hp.getId());
                Map<String, Object> variables =hp.getProcessVariables();
                myApply.setCategory(StringUtil.toNullOrString(variables.get(Constants.Process.CATEGORY)));
                myApply.setTitle(StringUtil.toNullOrString(variables.get(Constants.Process.TITLE)));
                myApply.setCreateDate(hp.getStartTime());
                myApply.setProcessDefinitionName(hp.getProcessDefinitionName());
                myApply.setProcessDefinitionKey(hp.getProcessDefinitionKey());
                myApply.setProcessDefinitionId(hp.getProcessDefinitionId());
                myApplys.add(myApply);


            }
        }
        List<Announcement> list = announcementService.selectLimitShow();
        model.addAttribute("list",list);
        model.addAttribute("applys",myApplys);
        model.addAttribute("user",map);
        return "main";
    }
}
