package edu.taru.office.mapper;

import edu.taru.office.pojo.Announcement;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnnouncementMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Announcement record);

    int insertSelective(Announcement record);

    Announcement selectByPrimaryKey(Integer id);

    /**
     * 查询公告列表
     * @return
     */
    List<Announcement> selectList();

    /**
     * 首页展示公告（分页）
     * @return
     */
    List<Announcement> selectLimitShow();

    int updateByPrimaryKeySelective(Announcement record);

    int updateByPrimaryKeyWithBLOBs(Announcement record);

    int updateByPrimaryKey(Announcement record);

    /**
     * 添加公告
     * @param title
     * @param content
     * @return
     */
    int insertAnnouncement(@Param("title") String title, @Param("content")  String content);
}