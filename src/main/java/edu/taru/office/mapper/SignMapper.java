package edu.taru.office.mapper;

import edu.taru.office.pojo.Sign;

import java.util.List;

/**
 * Created by HuKang on 2018/5/15.
 */
public interface SignMapper {
    /**
     * 查询签到列表
     * @param uid
     * @return
     */
    List<Sign> qryUserSignIn (Integer uid);
    /**
     * 判断是否已签到
     * @param uid
     * @return
     */
    Sign selectSignIn(Integer uid);
    /**
     * 签到
     * @param uid
     */
    void SignIn(Integer uid);
    /**
     * 查询用户签退信息
     * @param uid
     * @return
     */
    List<Sign> qryUserSignBack(Integer uid);

    /**
     * 判断是否已签退
     * @param uid
     * @return
     */
    Sign selectSignBack(Integer uid);

    /**
     * 签退
     * @param uid
     */
    void SignBack(Integer uid);

    /**
     * 查询考勤信息
     * @return
     */
    List<Sign> selectList ();
}
