package edu.taru.office.mapper;

import edu.taru.office.pojo.Leave;
import org.apache.ibatis.annotations.Param;

public interface LeaveMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Leave record);

    int insertSelective(Leave record);

    Leave selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Leave record);

    int updateByPrimaryKey(Leave record);

    int  updateResultByKey(@Param("result") String result, @Param("id") Integer id);
}