package edu.taru.office.mapper;

import edu.taru.office.pojo.Dept;

import java.util.List;

public interface DeptMapper {
    int deleteByPrimaryKey(Integer deptid);

    int insert(Dept record);

    int insertSelective(Dept record);

    Dept selectByPrimaryKey(Integer deptid);

    int updateByPrimaryKeySelective(Dept record);

    int updateByPrimaryKey(Dept record);

    /**
     * 查询部门列表
     * @return
     */
    List<Dept> selectList();

    /**
     * 添加部门(查询重复)
     * @param deptname
     * @return
     */
    int selectRepeat(String deptname);

    /**
     * 部门修改是对部门名称是否重复进行判断
     * @param dept
     * @return
     */
    int updateRepeat(Dept dept);

    /**
     * 根据用户ID关联查询所在部门
     * @param uid
     * @return
     */
    Dept selectDeptByUserId(Integer uid);
}