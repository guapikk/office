layui.config({
    base : "/office/static/js/"
}).use(['form','layer','jquery','laypage'],function(){
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;
    //操作
    $("body").on("click",".processAdd_btn",function(){  //添加新流程
        var index = layui.layer.open({
            title : "部署新流程",
            type : 2,
            content : '/view/process/addProcess',
            success : function(layero, index){

            }
        })
        //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
        $(window).resize(function(){
            layui.layer.full(index);
        })
        layui.layer.full(index);
    });
    $("body").on("click",".deploy_process",function(){  //重新部署流程
        var _this = $(this);
        layer.confirm('确定重新部署此流程？',{icon:3, title:'提示信息'},function(index){
            var processName= _this.attr("value")
            console.log(processName)
            $.ajax({
                type:'GET',
                data:{filename:processName},
                dataType:'json',
                url:'/process/deploy',
                success:function (rs) {
                    if(rs.status == '200'){
                        top.layer.msg(rs.message);
                        parent.location.reload();
                    }else if(rs.status == '404'){
                        top.layer.msg(rs.message);
                        parent.location.reload();
                    }else{
                        top.layer.msg(rs.message);
                        parent.location.reload();
                    }
                }
            })
        });
    })
    $("body").on("click",".deployProcess",function(){  //部署新流程
        layer.confirm('确定部署此流程？',{icon:3, title:'提示信息'},function(index){
            var processName= $("#resourceName").val();
            console.log(processName)
            $.ajax({
                type:'GET',
                data:{filename:processName},
                dataType:'json',
                url:'/process/deploy',
                success:function (rs) {
                    if(rs.status == '200'){
                        top.layer.msg(rs.message);
                        parent.location.reload();
                    }else if(rs.status == '404'){
                        top.layer.msg(rs.message);
                        parent.location.reload();
                    }else{
                        top.layer.msg(rs.message);
                        parent.location.reload();
                    }
                }
            })
        });
    })
    function newsList(that){
        //渲染数据
        function renderDate(data,curr){
            var dataHtml = '';
            if(!that){
                currData = newsData.concat().splice(curr*nums-nums, nums);
            }else{
                currData = that.concat().splice(curr*nums-nums, nums);
            }
            if(currData.length != 0){
                for(var i=0;i<currData.length;i++){
                    dataHtml += '<tr>'
                        +'<td><input type="checkbox" name="checked" lay-skin="primary" lay-filter="choose"></td>'
                        +'<td align="left">'+currData[i].newsName+'</td>'
                        +'<td>'+currData[i].newsAuthor+'</td>';
                    if(currData[i].newsStatus == "待审核"){
                        dataHtml += '<td style="color:#f00">'+currData[i].newsStatus+'</td>';
                    }else{
                        dataHtml += '<td>'+currData[i].newsStatus+'</td>';
                    }
                    dataHtml += '<td>'+currData[i].newsLook+'</td>'
                        +'<td><input type="checkbox" name="show" lay-skin="switch" lay-text="是|否" lay-filter="isShow"'+currData[i].isShow+'></td>'
                        +'<td>'+currData[i].newsTime+'</td>'
                        +'<td>'
                        +  '<a class="layui-btn layui-btn-mini "><i class="iconfont icon-edit"></i> 编辑</a>'
                        +  '<a class="layui-btn layui-btn-normal layui-btn-mini news_collect"><i class="layui-icon">&#xe600;</i> 收藏</a>'
                        +  '<a class="layui-btn layui-btn-danger layui-btn-mini news_del" data-id="'+data[i].newsId+'"><i class="layui-icon">&#xe640;</i> 删除</a>'
                        +'</td>'
                        +'</tr>';
                }
            }else{
                dataHtml = '<tr><td colspan="8">暂无数据</td></tr>';
            }
            return dataHtml;
        }

        //分页
        var nums = 13; //每页出现的数据量
        if(that){
            newsData = that;
        }
        laypage({
            cont : "page",
            pages : Math.ceil(newsData.length/nums),
            jump : function(obj){
                $(".news_content").html(renderDate(newsData,obj.curr));
                $('.news_list thead input[type="checkbox"]').prop("checked",false);
                form.render();
            }
        })
    }
})
