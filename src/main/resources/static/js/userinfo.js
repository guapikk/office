var areaData = address;
var $form;
var form;
var $;
layui.config({
	base : "/office/static/js/"
}).use(['form','layer','laydate','element','layer','upload'],function(){
	form = layui.form();
	var layer = parent.layer === undefined ? layui.layer : parent.layer;
		var $ = layui.jquery;
         upload = layui.upload;
		/*upload.render({
            elem: '.userFaceBtn',
            url: '/common/upload',
            method : "post",  //此处是为了演示之用，实际使用中请将此删除，默认用post方式提交
            done: function(res, index, upload){
                if(res.status == '200'){
                    layer.msg(res.msg);
                    $('#userFace').attr('src',"/static/images/"+res.data.imageName);
                    //$(".userAvatar").attr("src","/exam/static/images/"+res.data[0].src);
                }else{
                    layer.msg(res.msg);
                }

            },
            error: function(){
                //请求异常回调
                layer.msg("系统异常！");
            }
        })*/
		$form = $('form');
		laydate = layui.laydate;


        //添加验证规则
        var id =$("#oldPwds").val();
        form.verify({
            oldPwd : function(value, item){
                if(value != id){
                    return "密码错误，请重新输入！";
                }
            },
            newPwd : function(value, item){
                if(value.length < 6){
                    return "密码长度不能小于6位";
                }
            },
            confirmPwd : function(value, item){
                if(!new RegExp($("#oldPwd").val()).test(value)){
                    return "两次输入密码不一致，请重新输入！";
                }
            }
        })
        //提交个人资料
        form.on("submit(changeUser)",function(data){
        	var index = layer.msg('提交中，请稍候',{icon: 16,time:false,shade:0.8});
            setTimeout(function(){
                layer.close(index);
                layer.msg("提交成功！");
            },2000);
        	return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        })

        

        //修改密码
        form.on("submit(changePwd)",function(data){
        	var index = layer.msg('提交中，请稍候',{icon: 16,time:false,shade:0.8});
            setTimeout(function(){
                $.ajax({
                    url: '/user/changePwd',
                    type:"POST",
                    data:$("#changePwd").serialize(),
                    dataType:"json",
                    cache: false,
                    success:function(rs){
                        if(rs.status == '200'){
                            layer.msg(rs.message);
                            window.location.reload();

                        }else{
                            layer.msg(rs.message)
                            window.location.reload();
                        }
                    },
                });
            },2000);
        	return false; //  阻止表单跳转。如果需要表单跳转，去掉这段即可。
        })
    $("body").on("click",".userFaceBtn",function(){  //上传
        var formData = new FormData();
        formData.append("file", $("#userFace")[0].files[0]);
        $.ajax({
            url: '/common/upload',
            type: 'post',
            data: formData,
            processData: false,
            contentType: false,
            success: function (msg) {
                alert(msg.status);
            }
        });
    })

})

function preview(file) {
    var prevDiv = document.getElementById('preview');
    if (file.files && file.files[0]) {
        var reader = new FileReader();
        reader.onload = function (evt) {
            prevDiv.innerHTML = '<img class="layui-circle" style="width: 120px ;height: 120px" src="' + evt.target.result + '" name="" />';
        }
        reader.readAsDataURL(file.files[0]);
    } else {
        prevDiv.innerHTML = '<div class="img" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src=\'' + file.value + '\'"></div>';
    }
}