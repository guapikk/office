/**
 * Created by HuKang on 2018/4/22.
 */
layui.config({
    base : "/static/js/"
}).use(['form','layer','jquery','laypage','tree'],function(){
        var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery;
        console.log("加载js发送ajax");

            $.ajax({
                url: '/url/qryMenuTree',
                type:"GET",
                dataType:"json",
                cache: false,
                success:function(rs){
                    if(rs.status == '200'){

                        layui.tree({
                            elem: '#demo', //传入元素选择器
                            nodes:rs.data
                        });


                    }
                }
            });

            $.ajax({
                url: '/url/qryRoleExistTree',
                type:"GET",
                data:$("#insertUrlid").serialize(),
                dataType:"json",
                cache: false,
                success:function(rs){
                    if(rs.status == '200'){

                        layui.tree({
                            elem: '#demo1', //传入元素选择器
                            nodes:rs.data
                        });


                    }
                }
            });













    //提交角色分配菜单
    $(".allot_Menu").click(function(){
        var $checkbox = $('.news_list tbody input[type="checkbox"][name="checked"]');
        var $checked = $('.news_list tbody input[type="checkbox"][name="checked"]:checked');
        if($checkbox.is(":checked")){
            layer.confirm('确定选中的信息？',{icon:3, title:'提示信息'},function(index){
                var index = layer.msg('分配角色中，请稍候',{icon: 16,time:false,shade:0.8});
                setTimeout(function(){
                    $.ajax({
                        url: '/role/insertUrl',
                        type:"POST",
                        data:$("#insertUrlid").serialize(),
                        dataType:"json",
                        cache: false,
                        success:function(rs){
                            if(rs.status == '200'){
                                top.layer.msg(rs.message);
                                parent.location.reload();
                            }else{
                                top.layer.msg(rs.message)
                                parent.location.reload();
                            }
                        },
                    });
                },1000);
            })
        }else{
            layer.msg("请选择需要分配角色的用户");
        }

    })

    //全选
    form.on('checkbox(allChoose)', function(data){
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        child.each(function(index, item){
            item.checked = data.elem.checked;
        });
        form.render('checkbox');
    });
    //通过判断文章是否全部选中来确定全选按钮是否选中
    form.on("checkbox(choose)",function(data){
        var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
        var childChecked = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"]):checked')
        if(childChecked.length == child.length){
            $(data.elem).parents('table').find('thead input#allChoose').get(0).checked = true;
        }else{
            $(data.elem).parents('table').find('thead input#allChoose').get(0).checked = false;
        }
        form.render('checkbox');
    })



    //操作 修改
    $("body").on("click",".news_edit",function(){  //编辑
        layer.alert('您点击了文章编辑按钮，由于是纯静态页面，所以暂时不存在编辑内容，后期会添加，敬请谅解。。。',{icon:6, title:'用户修改'});
    })

    //操作  删除
    $("body").on("click",".news_del",function(){  //删除
        var _this = $(this);
        layer.confirm('确定删除此信息？',{icon:3, title:'提示信息'},function(index){
            //_this.parents("tr").remove();
            for(var i=0;i<newsData.length;i++){
                if(newsData[i].newsId == _this.attr("data-id")){
                    newsData.splice(i,1);
                    newsList(newsData);
                }
            }
            layer.close(index);
        });
    });
    function newsList(that){
        //渲染数据
        function renderDate(data,curr){
            var dataHtml = '';
            if(!that){
                currData = newsData.concat().splice(curr*nums-nums, nums);
            }else{
                currData = that.concat().splice(curr*nums-nums, nums);
            }
            if(currData.length != 0){
                for(var i=0;i<currData.length;i++){
                    dataHtml += '<tr>'
                        +'<td><input type="checkbox" name="checked" lay-skin="primary" lay-filter="choose"></td>'
                        +'<td align="left">'+currData[i].newsName+'</td>'
                        +'<td>'+currData[i].newsAuthor+'</td>';
                    if(currData[i].newsStatus == "待审核"){
                        dataHtml += '<td style="color:#f00">'+currData[i].newsStatus+'</td>';
                    }else{
                        dataHtml += '<td>'+currData[i].newsStatus+'</td>';
                    }
                    dataHtml += '<td>'+currData[i].newsLook+'</td>'
                        +'<td><input type="checkbox" name="show" lay-skin="switch" lay-text="是|否" lay-filter="isShow"'+currData[i].isShow+'></td>'
                        +'<td>'+currData[i].newsTime+'</td>'
                        +'<td>'
                        +  '<a class="layui-btn layui-btn-mini "><i class="iconfont icon-edit"></i> 编辑</a>'
                        +  '<a class="layui-btn layui-btn-normal layui-btn-mini news_collect"><i class="layui-icon">&#xe600;</i> 收藏</a>'
                        +  '<a class="layui-btn layui-btn-danger layui-btn-mini news_del" data-id="'+data[i].newsId+'"><i class="layui-icon">&#xe640;</i> 删除</a>'
                        +'</td>'
                        +'</tr>';
                }
            }else{
                dataHtml = '<tr><td colspan="8">暂无数据</td></tr>';
            }
            return dataHtml;
        }

        //分页
        var nums = 13; //每页出现的数据量
        if(that){
            newsData = that;
        }
        laypage({
            cont : "page",
            pages : Math.ceil(newsData.length/nums),
            jump : function(obj){
                $(".news_content").html(renderDate(newsData,obj.curr));
                $('.news_list thead input[type="checkbox"]').prop("checked",false);
                form.render();
            }
        })
    }
})

